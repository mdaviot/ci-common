#!/usr/bin/env bash

script_full_path=$(dirname "$0")

cd $script_full_path/dev-tools
git fetch --tags

LAST_TAG=$(git tag --sort=creatordate | tail -1)
if git diff --exit-code $LAST_TAG > /dev/null ; then
  echo "already at latest tag $LAST_TAG"
  exit 1
else
  git config --add advice.detachedHead false
  git checkout $LAST_TAG
  cd ..
  git submodule set-branch --branch $LAST_TAG -- dev-tools
fi
